Rails.application.routes.draw do
  get 'home/index'
  get 'about', to: 'pages#about'
  resources :articles
  root 'pages#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
